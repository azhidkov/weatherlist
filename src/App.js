import React, { useEffect, useRef, useState } from "react";
import "./style.css";

const App = () => {
  const mapRef = useRef(null);
  const myPlacemark = useRef(null);

  const [weatherData, setWeatherData] = useState();

  const ymaps = window.ymaps;

  const getWeatherFromAPI = async (coords) => {
    const data = await fetch(
      `https://api.openweathermap.org/data/2.5/forecast?lat=${coords[0]}&lon=${coords[1]}&appid=9a0c326cb63df6c1dd4c65862b3fdf63`
    );
    const response = await data.json();
    const date = response.list[0].dt_txt.split(" ")[0];
    console.log(date);
    setWeatherData(response);
  };

  const getAddress = (coords) => {
    console.log(coords); // coords
    getWeatherFromAPI(coords);
    myPlacemark.current.properties.set("iconCaption", "поиск...");
    ymaps.geocode(coords).then(function (res) {
      const firstGeoObject = res.geoObjects.get(0);

      myPlacemark.current.properties.set({
        iconCaption: [
          firstGeoObject.getLocalities().length
            ? firstGeoObject.getLocalities()
            : firstGeoObject.getAdministrativeAreas(),
          firstGeoObject.getThoroughfare() || firstGeoObject.getPremise(),
        ]
          .filter(Boolean)
          .join(", "),
        balloonContent: firstGeoObject.getAddressLine(),
      });
    });
  };

  const createPlacemark = (coords) => {
    return new ymaps.Placemark(
      coords,
      {
        iconCaption: "поиск...",
      },
      {
        preset: "islands#violetDotIconWithCaption",
        draggable: true,
      }
    );
  };

  useEffect(() => {
    ymaps.ready(() => {
      let placemark;
      const myMap = new ymaps.Map(mapRef.current, {
        center: [55.753994, 37.622093],
        zoom: 9,
        controls: [],
      });

      const mySearchControl = new ymaps.control.SearchControl({
          options: {
            noPlacemark: true,
          },
        }),
        mySearchResults = new ymaps.GeoObjectCollection(null, {
          hintContentLayout:
            ymaps.templateLayoutFactory.createClass("$[properties.name]"),
        });
      myMap.controls.add(mySearchControl);
      myMap.geoObjects.add(mySearchResults);
      mySearchResults.events.add("click", function (e) {
        e.get("target").options.set("preset", "islands#redIcon");
      });
      mySearchControl.events
        .add("resultselect", function (e) {
          var index = e.get("index");
          mySearchControl.getResult(index).then(function (res) {
            mySearchResults.add(res);
            console.log(res.geometry.getCoordinates()); //coords
            getWeatherFromAPI(res.geometry.getCoordinates());
          });
        })
        .add("submit", function () {
          mySearchResults.removeAll();
        });

      myMap.events.add("click", function (e) {
        const coords = e.get("coords");

        if (placemark) {
          placemark.geometry.setCoordinates(coords);
          myPlacemark.current = placemark;
        } else {
          placemark = createPlacemark(coords);

          myPlacemark.current = placemark;
          myMap.geoObjects.add(placemark);
          placemark.events.add("dragend", function () {
            getAddress(placemark.geometry.getCoordinates());
          });
        }
        console.log(placemark);
        myPlacemark.current = placemark;
        getAddress(coords);
      });
    });
  }, []);
  const city =
    myPlacemark?.current?.properties?._data?.iconCaption.split(",")[0];
  console.log(city);
  let weatherIcon;
  switch (weatherData?.list[0]?.weather[0]?.main) {
    case "Clouds":
      weatherIcon = <ion-icon name="cloud-outline"></ion-icon>;
      break;
    case "Rain":
      weatherIcon = <ion-icon name="rainy-outline"></ion-icon>;
      break;
    case "Thunderstorm":
      weatherIcon = <ion-icon name="thunderstorm-outline"></ion-icon>;
      break;
    case "Snow":
      weatherIcon = <ion-icon name="snow-outline"></ion-icon>;
      break;
    case "Clear":
      weatherIcon = <ion-icon name="sunny-outline"></ion-icon>;
      break;
    default:
      alert("Нет таких значений");
  }
  console.log(weatherData);
  return (
    <>
      <div className="app-container">
        <div className="app-map" ref={mapRef}></div>
        <div className="app-weather-container">
          <div className="app-weather-content">
            <div className="app-weather-flex">
              <div className="app-weather-left">
                <span>{city}</span>
                <div className="app-weather-icon">
                  <span>
                    {Math.round(weatherData?.list[0]?.main?.temp - 273)}°C
                  </span>
                  {weatherIcon}
                </div>
                <span>
                  По ощущению:
                  {Math.round(weatherData?.list[0]?.main?.feels_like - 273)}
                </span>
                <span>Ветер: {weatherData?.list[0]?.wind?.speed} м/с</span>
                <span>
                  Давление:
                  {Math.round(weatherData?.list[0]?.main?.pressure * 0.750064)}
                  мм рт. ст.
                </span>
                <span>Влажность: {weatherData?.list[0].main?.humidity} %</span>
              </div>
              <div className="app-weather-right">
                <div className="app-weather-times">
                  <span>00:00</span>
                  <span>03:00</span>
                </div>
                <div className="app-weather-wind">
                  <span>Скорость ветра</span>
                </div>
              </div>
            </div>
            <div className="app-weather-bottom">
              <span>Сегодня</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default App;
